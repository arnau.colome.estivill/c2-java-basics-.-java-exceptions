package excepcion;

public class Excepcion extends Exception {

	private int codigoExcepcion;

	public Excepcion(int codigoExcepcion) {
		super();
		this.codigoExcepcion=codigoExcepcion;
	}
	
	
	@Override
	public String getMessage() {
		
		String mensaje="";
		
		switch(codigoExcepcion) {
			case 1:
				mensaje = "numero a tiene que ser negativo";
				break;
			
			case 2:
				mensaje = "numeros minimos de 2 cifras";
				break;
			
			case 3:
				mensaje = "numero b tiene que ser positivo";
				break;
			
			case 4:
				mensaje = "numero b tiene que ser negativo ";
				break;
			
			case 5:
				mensaje = "numero a tiene que ser positivo";
				break;
			
			case 6:
				mensaje = "El primer numero no puede ser 0";
				break;
				
			case 7:
				mensaje = "El segundo numero no puede ser 0";
				break;
		}
		return mensaje;
		
	}
	
}
