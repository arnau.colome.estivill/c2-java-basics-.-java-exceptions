package dto;
import excepcion.Excepcion;

public class Operaciones {

	public double a;
	public double b;
	
	public Operaciones() {
		this.a=0;
		this.b=0;
	}



	//suma
	public static void suma(double a, double b) {
		
		try {
			if(a < 0) {
		
				double result = a + b;
		
				System.out.println("Suma de " + a + " + " + b + ": " + result);
				System.out.println("");

		}
		else {
			throw new Excepcion(1);
		}
	}
	catch (Excepcion ex) {
		System.out.println(ex.getMessage());
	}
			
	}
	
	//resta
	public static void resta(double a, double b) {
		try {
			if((a <= -10) || (a >= 10) && (b <= -10)||(b >= 10)) {
			
			double result = a - b;
			
			System.out.println("Resta de " + a + " - " + b + ": " + result);
			System.out.println("");
		
			}
			else {
				throw new Excepcion(2);
			}
		}
		catch (Excepcion ex) {
			System.out.println(ex.getMessage());
		}
		
	}
	
	//multiplicacion
	public static void multiplicacion(double a, double b) {

		try {
			if(b >= 0) {
		
				double result = a * b;
				
				System.out.println("Multiplicacion de " + a + " * " + b + ": " + result);
				System.out.println("");
		
			}
			else {
				throw new Excepcion(3);
			}
		}
		catch (Excepcion ex) {
			System.out.println(ex.getMessage());
		}
		

	}
	
	//potencia
	public static void potencia(double a, double b) {

		try {
			if(b < 0) {
				
				double result = Math.pow(a, b);
				
				System.out.println("Potencia de " + a + " y " + b + ": " + result);
				System.out.println("");
		
			}
			else {
				throw new Excepcion(4);
			}
		}
		catch (Excepcion ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	//raiz cuadrada
	public static void raizCuadrada(double a, double b) {
		

		try {
			if(a >= 0) {
		
				double resultA = Math.sqrt(a);
				double resultB = Math.sqrt(b);	
			
			System.out.println("Raiz Cuadrada de " + a + " : " + resultA);
			System.out.println("Raiz Cuadrada de " + b + " : " + resultB);
			
			}
			else {
				throw new Excepcion(4);
			}
		}
		catch (Excepcion ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	//raiz cubica
	public static void raizCubica(double a, double b) {
		
		
		try {
			if (a != 0) {
		
				double resultA = Math.cbrt(a);
				double resultB = Math.cbrt(b);
		
				System.out.println("Raiz C�bica de " + a + " : " + resultA);
				System.out.println("Raiz C�bica de " + b + " : " + resultB);

			}
			else {
				throw new Excepcion(6);
			}
		}
		catch (Excepcion ex) {
			System.out.println(ex.getMessage());
		}
	}

	//division
	public static void division(double a, double b) {

		try {
			if (b != 0) {
				double result = a / b;
		
				System.out.println("Divisi�n de " + a + " / " + b + ": " + result);
				System.out.println("");

		}
		else {
			throw new Excepcion(7);
		}
			
		}
		catch (Excepcion ex) {
			System.out.println(ex.getMessage());
		}
	}

}
