package views;
import dto.Operaciones;
import java.util.Scanner;

public class Vistas {

	Scanner scan = new Scanner(System.in);
	
	public static void menu() {
		boolean salir = false;
	       int opcion; 
	       Scanner scan = new Scanner(System.in);
	       
	       System.out.println("Numero a: ");
	       double numA = scan.nextDouble();
			
	       System.out.println("Numero b: ");
	       double numB = scan.nextDouble();
	       
	       while(!salir){
	            
	           System.out.println("1. Suma");
	           System.out.println("2. Resta");
	           System.out.println("3. Multiplicaci�n");
	           System.out.println("4. Potencia");
	           System.out.println("5. Raiz cuadrada");
	           System.out.println("6. Raiz c�bica");
	           System.out.println("7. Divisi�n");
	           System.out.println("8. Salir");
	            
	           System.out.println("Escribe una de las opciones");
	           opcion = scan.nextInt();
	            
	           switch(opcion){
	               case 1:
	                   	System.out.println("== SUMA ==");
	                    Operaciones.suma(numA, numB);
	                   	break;
	               case 2:
	                   	System.out.println("== RESTA ==");
	                   	Operaciones.resta(numA, numB);
	                   	break;
	                case 3:
	                	System.out.println("== MULTIPLICACI�N ==");
	                	Operaciones.multiplicacion(numA, numB);
	               		break;
	                case 4:
	                	System.out.println("== POTENCIA ==");
	                	Operaciones.potencia(numA, numB);
	               		break;
	                case 5:
	                	System.out.println("== RAIZ CUADRADA ==");
	                	Operaciones.raizCuadrada(numA, numB);
	               		break;
	                case 6:
	                	System.out.println("== RAIZ C�BICA ==");
	                	Operaciones.raizCubica(numA, numB);
	               		break;
	                case 7:
	                	System.out.println("== DIVISI�N ==");
	                	Operaciones.division(numA, numB);
	               		break;
	                case 8:
	                	salir=true;
	                	break;
	                default:
	                	System.out.println("Solo n�meros entre 1 y 8");
	           }
	            
	       }
	       scan.close();
	}
	
}
