import exception.Excepcion;
public class mainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int num;
		
		try {
			System.out.println("Mensaje mostrado por pantalla");
			num = 1;
			
			if (num == 1) {
				throw new Excepcion(1);
			}
			else if (num == 2) {
				throw new Excepcion(2);
			}

			
		}
		
		catch (Excepcion ex) {
			System.out.println(ex.getMessage());
		}
		finally {
			System.out.println("Programa terminado");
		}
		
	}

}
