package exception;

public class Excepcion extends Exception {
	
	private int codigoExcepcion;

	public Excepcion(int codigoExcepcion) {
		super();
		this.codigoExcepcion=codigoExcepcion;
	}
	
	@Override
	public String getMessage() {
		
		String mensaje="";
		
		switch(codigoExcepcion) {
		case 1:
				mensaje="Excepcion capturada con mensaje: Esto es un objecto Exception";
				break;
		case 2:
				mensaje="Excepcion capturada con mensaje: Esto es un objecto Exception 2";
				break;
		}
		return mensaje;
		
	}
	
	
	
	
}
