import dto.random;
import excepcion.excepcion;
public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*
			Escribe un programa , utilizando para ello el paradigma de POO, que genere
			un n�mero aleatorio e indique si el n�mero generado es par o impar. El
			programa utilizar� para ello el lanzamiento de una excepci�n.
			
			Recomendaciones:
			1. El programa utiliza la clase Random() para obtener un n�mero aleatorio
			entre 0 y 999 (por poner un rango cualquiera).
			2. Se determina si el n�mero es par o impar y se lanza una excepci�n con el
			correspondiente mensaje para indicarlo (se limitar� a mostrar el mensaje
			asociado a la excepci�n capturada).
		*/
		
		try {
			
			int numRandom = random.Random();
			System.out.println("Generando numero aleatorio...");
			if (numRandom % 2 == 0) {
				throw new excepcion(1);
			}
			else {
				throw new excepcion(2);
			}

		}
		catch (excepcion ex) {
			System.out.println(ex.getMessage());
		}
		
	}

}
