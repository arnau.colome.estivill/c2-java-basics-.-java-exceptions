package excepcion;

public class excepcion extends Exception{

	private int codigoExcepcion;

	public excepcion(int codigoExcepcion) {
		super();
		this.codigoExcepcion=codigoExcepcion;
	}
	
	
	@Override
	public String getMessage() {
		
		String mensaje="";
		
		switch(codigoExcepcion) {
			case 1:
					mensaje="Es par";
					break;
			case 2:
					mensaje="Es impar";
					break;
		}
		return mensaje;
		
	}
	
}
