package dto;

import java.util.Random;

public class Password {
	
	/*
	 	Haz una clase llamada Password que siga las siguientes condiciones:
		• Que tenga los atributos longitud y contraseña . Por defecto, la longitud sera de 8.
		• Los constructores serán los siguiente:
		✓ Un constructor por defecto.
		✓ Un constructor con la longitud que nosotros le pasemos. Generara una
		contraseña aleatoria con esa longitud.
		
		Los métodos que implementa serán:
		• esFuerte(): devuelve un booleano si es fuerte o no, para que sea fuerte debe
		tener mas de 2 mayúsculas, mas de 1 minúscula y mas de 5 números.
		• generarPassword(): genera la contraseña del objeto con la longitud que
		tenga.
		• Método get para contraseña y longitud.
		• Método set para longitud.
	*/
	
	
	// Que tenga los atributos longitud y contraseña . Por defecto, la longitud sera de 8.
	
	protected int longitud;
	protected String contrasena;
	
	/*
		• Los constructores serán los siguiente:
		✓ Un constructor por defecto.
		✓ Un constructor con la longitud que nosotros le pasemos. Generara una
		contraseña aleatoria con esa longitud.
	*/
	
	public Password() {
		this.longitud=10;
		this.contrasena=generarPassword(this.longitud);
	}
	
	public Password(int longitud) {
		this.longitud = longitud;
		this.contrasena = generarPassword(this.longitud); //llamara al codigo de generarPassword para crear una contraseña
	}

	// esFuerte(): devuelve un booleano si es fuerte o no, para que sea fuerte debe
	/// tener mas de 2 mayúsculas, mas de 1 minúscula y mas de 5 números.
	public boolean esFuerte() {
		boolean fuerte=false; // boolean final para determinar si es fuerte o no
		String contrasena=this.contrasena;
		int mayuscula = 0; // mas de 2 mayusculas
		int minuscula = 0; // mas de 1 minuscula 
		int numeros = 0; // mas de 5 numeros
		
		//tenemos que recorrer cada letra/numero
		//en este bloque me he inspirado en esta solucion pero adaptada al codigo 
		//https://stackoverflow.com/questions/40336374/how-do-i-check-if-a-java-string-contains-at-least-one-capital-letter-lowercase
		for (int i=00; i < contrasena.length();i++) {
			char ch = contrasena.charAt(i);
			if (Character.isUpperCase(ch)) { // mayusculas
				mayuscula++;
			}
			else if(Character.isLowerCase(ch)) { // minusculas
				minuscula++;
			}
			else if(Character.isDigit(ch)) { // numeros
				numeros++;
			}
			
		}
		
		if ((mayuscula >= 2) && (minuscula >=1) && (numeros >= 5)) {
			fuerte=true;
		}
		
		return fuerte;
	}
	
	
	// este bloque genera una contraseña con los caracteres char introducidos 
	public String generarPassword(int longitud) {

		char[] ch = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
	        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
	        'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	        'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
	        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	        'w', 'x', 'y', 'z' };
	    
	    char[] pass=new char[longitud];
	    Random random=new Random();
	    for (int i = 0; i < longitud; i++) {
	      pass[i]=ch[random.nextInt(ch.length)];
	    }
	    
	    return new String(pass);
		
	}

	
	// • Método get para contraseña y longitud.
	// • Método set para longitud.
	
	
	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public String getContrasena() {
		return contrasena;
	}

	@Override
	public String toString() {
		return "Password [longitud=" + longitud + ", contrasena=" + contrasena + "]";
	}
	
	

}
