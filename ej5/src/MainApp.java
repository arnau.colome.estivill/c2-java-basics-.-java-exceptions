import views.Vistas;

import java.util.Enumeration;
import java.util.Hashtable;

import dto.Password;
import excepcion.ExcepcionCustomizada;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int tama�oArrayPasswordsInt = 0;
		int longitudpasswordsInt = 0;
		
		try {
			String tama�oArrayPasswords = Vistas.pedirInformacion("Introduzca el tama�o del array de passwords");
			tama�oArrayPasswordsInt = Integer.parseInt(tama�oArrayPasswords);
			
			String longitudpasswords = Vistas.pedirInformacion("Introduzca la longitud de las contrase�as");
			longitudpasswordsInt = Integer.parseInt(longitudpasswords);
			
			if(tama�oArrayPasswordsInt < 0) {
				throw new ExcepcionCustomizada(01);
			}
			
			Password[] arrayPasswords = new Password[tama�oArrayPasswordsInt];
			
			Hashtable<String, Boolean> contrase�as=new Hashtable<String, Boolean>();
			
			for (int i = 0; i < tama�oArrayPasswordsInt; i++) {
				arrayPasswords[i] = new Password(longitudpasswordsInt);
				contrase�as.put(arrayPasswords[i].getContrasena(),arrayPasswords[i].esFuerte());
			}
			
			Enumeration<Boolean> enumeration = contrase�as.elements();
	  		Enumeration<String> llaves = contrase�as.keys();
	  		while (enumeration.hasMoreElements()) {
	  		  Vistas.mostrarMensaje(llaves.nextElement() + " " + enumeration.nextElement());
	  		}
			
			
		} catch (NumberFormatException e1) {
			Vistas.mostrarMensaje("Debe introducir un numero");
		} catch (ExcepcionCustomizada e2) {
			Vistas.mostrarMensaje(e2.getMessage());
		}
		



		
	}

}
