package views;

import javax.swing.JOptionPane;

public class Vistas {

	public static String pedirInformacion(String mensaje) {
		String tama�o = JOptionPane.showInputDialog(mensaje);
		return tama�o;
	}
	
	public static void mostrarMensaje(String mensaje) {
		JOptionPane.showMessageDialog(null, mensaje);
	}
}
